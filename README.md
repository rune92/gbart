#Gbart feladat
###A feladat megoldása során használt környezet
* Symfony 2.8.14
* PHP 5.6
* Composer
* Mysql
* PhpStorm

#Alkalmazás beüzemelése
##Repo klonozása
git clone https://rune92@bitbucket.org/rune92/gbart.git

#Vhost létrehozása apache esetén
```
sudo -i
cd /etc/apache2/sites-available/
vim gbart.conf
```
```
<VirtualHost *:80>
    ServerName gbart.tld
    #ServerAlias www.domain.tld

    DocumentRoot /var/www/gbart/web
    <Directory /var/www/gbart/web>
        AllowOverride None
        Order Allow,Deny
        Allow from All

        <IfModule mod_rewrite.c>
            Options -MultiViews
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*)$ app_dev.php [QSA,L]
        </IfModule>
    </Directory>

    # uncomment the following lines if you install assets as symlinks
    # or run into problems when compiling LESS/Sass/CoffeScript assets
    # <Directory /var/www/project>
    #     Options FollowSymlinks
    # </Directory>

    # optionally disable the RewriteEngine for the asset directories
    # which will allow apache to simply reply with a 404 when files are
    # not found instead of passing the request into the full symfony stack
    <Directory /var/www/gbart/web/bundles>
        <IfModule mod_rewrite.c>
            RewriteEngine Off
        </IfModule>
    </Directory>
    ErrorLog /var/log/apache2/gbart_error.log
    CustomLog /var/log/apache2/gbart_access.log combined
</VirtualHost>

```
#Szimbolikus link kialakítása
```
ln -s /etc/apache2/sites-available/gbart.conf /etc/apache2/sites-enabled/gbart.conf
```
#host engedélyezése az /etc/hosts fájlban
```
127.0.0.1       gbart.tld
```
##Jogokat adunk az app/logs és az app/cache mappáknak, hogy írhatóak legyenek
```
HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var/cache var/logs var/session
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var/cache var/logs var/session
```

##Létrehozzuk a paramaters.yml és kitöltjük a példa alapján
sudo cp app/config/parameters.yml.dist app/config/parameters.yml

###példa
```
# This file is auto-generated during the composer install
parameters:
    database_host: 127.0.0.1
    database_port: null
    database_name: symfony
    database_user: user
    database_password: pass
    mailer_transport: sendmail
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    secret: 86bea9cdcf7168e5bed6c488f3880d73ac700845
    email: youremail

```
##Composer-el feltelepítjük a vendort
```
curl -sS https://getcomposer.org/installer | php
php composer.phar install
```

##Adatbázis létrehozása és a séma létrehozása
```
php app/console doctrine:database:create
php app/console doctrine:schema:update --force
```

##Alkalmazás működés

Az alkalmazás bejelentkezés után érhető el, viszont a bejelntkezéshez be kell regisztrálni egy usert.
Ehhez használjuk a Symfony alap shell parancsát:
```
php app/console fos:user:create
```
A parancs ezek után elkéri a user adatok, amit beütve már van is felhasználónk.
Most már betudunk lépni a felületre, ahol alap esetben csak egy gomb látható, megnyomva elkészül a user első Todo listája.
A listát lehetőség van törölni is a remove gombbal.
A to list linkre kattintva átnavigálunk a lista gridjéhez, ahol majd megfognak jelenni a lista elemei, de egyelőre csak egy input
mező látható, ahova teendőt kell beírni, majd a save gombra kattintva rögzíteni. Miután ez megtörtént láthatóvá válik az első teendő.
Ezt lehetőség van készre állítani, illetve visszavonni és törölni a listából.




