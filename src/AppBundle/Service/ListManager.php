<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 2016.12.06.
 * Time: 20:43
 */

namespace AppBundle\Service;

use AppBundle\Entity\TodoItem;
use AppBundle\Entity\TodoList;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Session\Session;

class ListManager
{
    /**
     * @var EntityManager
     */
    private $_em;

    /**
     * @var Logger
     */
    private $_logger;

    /**
     * @var Session
     */
    private $_session;

    /**
     * ListManager constructor.
     * @param EntityManager $em
     * @param Logger $logger
     * @param Session $session
     */
    public function __construct(EntityManager $em, Logger $logger, Session $session)
    {
        $this->_em = $em;
        $this->_logger = $logger;
        $this->_session = $session;
    }

    public function getTodoLists()
    {
        /** @var TodoList $lists */
        $lists = $this->_em->getRepository('AppBundle:TodoList')->findAll();

        return $lists;
    }

    /**
     * @param $id
     * @return TodoList
     */
    public function getList($id)
    {
        /** @var TodoList $list */
        $list = $this->_em->getRepository('AppBundle:TodoList')->findOneBy(['id' => $id]);
        return $list;
    }

    /**
     * @param $id
     * @return TodoItem
     */
    public function getItem($id)
    {
        /** @var TodoItem $item */
        $item = $this->_em->getRepository('AppBundle:TodoItem')->findOneBy(['id' => $id]);

        return $item;
    }

    /**
     * @param $id
     * @return TodoList
     */
    public function removeList($id)
    {
        /** @var TodoList $list */
        $list = $this->_em->getRepository('AppBundle:TodoList')->findOneBy(['id' => $id]);

        try {
            $this->_em->remove($list);
            $this->_em->flush();
        } catch (\Exception $e) {
            $this->_logger->addCritical("Error in remove list entity: {$e->getMessage()}");
            $this->_session->getFlashBag()->set('error', 'Error in remove method!');
        }

        return $list;
    }

    /**
     * @param TodoItem $item
     * @return TodoItem
     */
    public function removeListItem($item)
    {
        try {
            $this->_em->remove($item);
            $this->_em->flush();
        } catch (\Exception $e) {
            $this->_logger->addCritical("Error in remove list entity: {$e->getMessage()}");
            $this->_session->getFlashBag()->set('error', 'Error in remove method!');
        }

        return $item;
    }

    /**
     * @param TodoList $list
     */
    public function saveListEntity(TodoList $list)
    {
        try {
            $this->_em->persist($list);
            $this->_em->flush();
        } catch (\Exception $e) {
            $this->_logger->addCritical("Error in saving list entity: {$e->getMessage()}");
            $this->_session->getFlashBag()->set('error', 'Error in saving method!');
        }
    }

    /**
     * @param TodoItem $item
     * @param $parent
     */
    public function saveListItemEntity(TodoItem $item, $parent)
    {
        $list = $this->getList($parent);
        $item->setParent($list);
        $this->saveItem($item);
    }

    /**
     * @param TodoItem $item
     */
    public function saveItem(TodoItem $item)
    {
        try {
            $this->_em->persist($item);
            $this->_em->flush();
        } catch (\Exception $e) {
            $this->_logger->addCritical("Error in saving list item entity: {$e->getMessage()}");
            $this->_session->getFlashBag()->set('error', 'Error in saving method!');
        }
    }
}