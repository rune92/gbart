<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 2016.12.05.
 * Time: 23:02
 */

namespace AppBundle\Entity;


interface TodoListInterface
{
    /**
     * @param TodoItem $item
     */
    public function addItem(TodoItem $item);

    /**
     * @param TodoItem $item
     * @return $this
     */
    public function removeItem(TodoItem $item);
}