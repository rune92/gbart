<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TodoList
 *
 * @ORM\Table(name="todo_list")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TodoListRepository")
 */
class TodoList implements TodoListInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="user", type="object")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createTime", type="datetime")
     */
    private $createTime;

    /**
     * @ORM\OneToMany(targetEntity="TodoItem", mappedBy="parent", cascade={"all"})
     */
    private $items;

    /**
     * TodoItem constructor.
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->setCreateTime(new \DateTime('now'));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \stdClass $user
     * @return TodoList
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set createTime
     *
     * @param \DateTime $createTime
     * @return TodoList
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * Get createTime
     *
     * @return \DateTime
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * Set items
     *
     * @param array $items
     * @return TodoList
     */
    public function setItems($items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param TodoItem $item
     */
    public function addItem(TodoItem $item)
    {
        $this->items[] = $item;
    }

    /**
     * @param TodoItem $item
     * @return $this
     */
    public function removeItem(TodoItem $item)
    {
        $this->items->removeElement($item);

        return $this;
    }
}
