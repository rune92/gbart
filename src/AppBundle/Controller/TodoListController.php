<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TodoItem;
use AppBundle\Entity\TodoList;
use AppBundle\Form\TodoItemType;
use AppBundle\Form\TodoListType;
use AppBundle\Service\ListManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class TodoListController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        /** @var ListManager $listManager */
        $listManager = $this->container->get('list.manager');
        $todoLists = $listManager->getTodoLists();

        $listForm = $this->createForm(TodoListType::class, null, [
            'action' => $this->generateUrl('newList'),
            'method' => 'POST',
        ]);

        return $this->render('AppBundle:TodoListController:index.html.twig', array(
            'lists' => $todoLists,
            'listForm' => $listForm->createView(),
        ));
    }

    /**
     * @Route("/newList", name="newList")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newListAction(Request $request)
    {
        /** @var ListManager $listManager */
        $listManager = $this->container->get('list.manager');
        $list = new TodoList();
        $list->setUser($this->getUser());

        $form = $this->createForm(TodoListType::class, $list);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listManager->saveListEntity($list);
        }

        return new RedirectResponse($this->generateUrl('homepage'));
    }

    /**
     * @Route("/list/{id}", requirements={"id" = "\d+"}, name="list")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($id)
    {
        /** @var ListManager $listManager */
        $listManager = $this->container->get('list.manager');
        $listItemForm = $this->createForm(TodoItemType::class, null, [
            'action' => $this->generateUrl('newListItem', array('id' => $id)),
            'method' => 'POST',
        ]);
        $list = $listManager->getList($id);

        return $this->render('AppBundle:TodoListController:list.html.twig', array(
            'list' => $list,
            'listItemForm' => $listItemForm->createView(),
        ));
    }

    /**
     * @Route("/removeList/{id}", requirements={"id" = "\d+"}, name="removeList")
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeListAction($id)
    {
        /** @var ListManager $listManager */
        $listManager = $this->container->get('list.manager');
        $listManager->removeList($id);

        return new RedirectResponse($this->generateUrl('homepage'));
    }

    /**
     * @Route("/newListItem/{id}", requirements={"id" = "\d+"}, name="newListItem")
     *
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function newListItemAction($id, Request $request)
    {
        /** @var ListManager $listManager */
        $listManager = $this->container->get('list.manager');

        $item = new TodoItem();
        $item->setUser($this->getUser());

        $form = $this->createForm(TodoItemType::class, $item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listManager->saveListItemEntity($item, $id);
        }

        return new RedirectResponse($this->generateUrl('list', ['id' => $id]));
    }

    /**
     * @Route("/changeItemStatus/{id}", requirements={"id" = "\d+"}, name="changeItemStatus")
     */
    public function changeItemStatusAction($id)
    {
        /** @var ListManager $listManager */
        $listManager = $this->container->get('list.manager');
        $item = $listManager->getItem($id);
        $item->changeStatus();
        $listManager->saveItem($item);
        /** @var TodoList $parent */
        $parent = $item->getParent();

        return new RedirectResponse($this->generateUrl('list', ['id' => $parent->getId()]));
    }

    /**
     * @Route("/removeListItem/{id}", requirements={"id" = "\d+"}, name="removeListItem")
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeListItemAction($id)
    {
        /** @var ListManager $listManager */
        $listManager = $this->container->get('list.manager');
        $item = $listManager->getItem($id);
        $listManager->removeListItem($item);
        /** @var TodoList $parent */
        $parent = $item->getParent();

        return new RedirectResponse($this->generateUrl('list', ['id' => $parent->getId()]));
    }

}
