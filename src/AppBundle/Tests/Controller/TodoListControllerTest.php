<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TodoListControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
    }

    public function testNewlist()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/newList');
    }

    public function testRemovelist()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/removeList');
    }

    public function testNewlistitem()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/newListItem');
    }

    public function testRemovelistitem()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/removeListItem');
    }

}
